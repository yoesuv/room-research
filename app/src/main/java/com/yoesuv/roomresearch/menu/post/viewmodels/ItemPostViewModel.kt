package com.yoesuv.roomresearch.menu.post.viewmodels

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.yoesuv.roomresearch.menu.post.models.PostModel

class ItemPostViewModel(postModel: PostModel): ViewModel() {

    val idPost: ObservableInt = ObservableInt(postModel.id!!)
    val title: ObservableField<String> = ObservableField(postModel.title!!)

}