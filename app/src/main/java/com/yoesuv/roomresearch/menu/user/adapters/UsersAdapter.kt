package com.yoesuv.roomresearch.menu.user.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.yoesuv.roomresearch.menu.user.models.UserModel
import com.yoesuv.roomresearch.utils.AdapterCallback

class UsersAdapter: ListAdapter<UserModel, UserViewHolder>(AdapterCallback.diffUsersCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}