package com.yoesuv.roomresearch.menu.user.models

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "geo")
data class GeoModel(
    @PrimaryKey(autoGenerate = true) val id:Int?,
    @ColumnInfo(name = "latitude") val lat: String?,
    @ColumnInfo(name = "longitude") val lng: String?
)