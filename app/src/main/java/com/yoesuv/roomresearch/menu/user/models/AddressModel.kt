package com.yoesuv.roomresearch.menu.user.models

import androidx.annotation.Keep
import androidx.room.*

@Keep
@Entity(tableName = "address")
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
data class AddressModel(
    @PrimaryKey(autoGenerate = true) val id:Int?,
    @ColumnInfo(name = "street") val street: String?,
    @ColumnInfo(name = "suite") val suite: String?,
    @ColumnInfo(name = "city") val city: String?,
    @ColumnInfo(name = "zip_code") val zipcode: String?,
    @Embedded(prefix = "address_geo") val geo: GeoModel?
)