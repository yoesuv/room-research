package com.yoesuv.roomresearch.menu.post.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yoesuv.roomresearch.data.db.DbPostRepository
import com.yoesuv.roomresearch.menu.post.models.PostModel

class PostsViewModel(context: Context): ViewModel() {

    private val dbPostRepo = DbPostRepository(context, viewModelScope)

    var dataPosts: MutableLiveData<MutableList<PostModel>?> = MutableLiveData()

    init {
        dbPostRepo.getPosts {
            dataPosts.postValue(it)
        }
    }

}