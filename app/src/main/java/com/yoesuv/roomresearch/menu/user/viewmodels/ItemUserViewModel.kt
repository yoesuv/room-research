package com.yoesuv.roomresearch.menu.user.viewmodels

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.yoesuv.roomresearch.menu.user.models.UserModel

class ItemUserViewModel(userModel: UserModel): ViewModel() {

    var id:ObservableInt = ObservableInt(userModel.id!!)
    var name:ObservableField<String> = ObservableField(userModel.name!!)
    var email:ObservableField<String> = ObservableField(userModel.email!!)

}