package com.yoesuv.roomresearch.menu.user.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.yoesuv.roomresearch.databinding.FragmentUserBinding
import com.yoesuv.roomresearch.menu.user.adapters.UsersAdapter
import com.yoesuv.roomresearch.menu.user.viewmodels.UsersViewModel
import com.yoesuv.roomresearch.utils.ViewModelFragmentFactory

class FragmentUsers: Fragment() {

    private lateinit var binding: FragmentUserBinding
    private val viewModel: UsersViewModel by viewModels { ViewModelFragmentFactory(context as Context) }

    private lateinit var usersAdapter: UsersAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.users = viewModel

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        usersAdapter = UsersAdapter()
        binding.rvUsers.apply {
            adapter = usersAdapter
        }
        viewModel.dataUsers.observe(viewLifecycleOwner, Observer {
            usersAdapter.submitList(it)
        })
    }

}