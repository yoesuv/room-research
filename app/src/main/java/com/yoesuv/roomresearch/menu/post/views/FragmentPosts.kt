package com.yoesuv.roomresearch.menu.post.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.yoesuv.roomresearch.databinding.FragmentPostBinding
import com.yoesuv.roomresearch.menu.post.adapters.PostsAdapter
import com.yoesuv.roomresearch.menu.post.viewmodels.PostsViewModel
import com.yoesuv.roomresearch.utils.ViewModelFragmentFactory

class FragmentPosts: Fragment() {

    private lateinit var binding: FragmentPostBinding
    private val viewModel: PostsViewModel by viewModels { ViewModelFragmentFactory(context as Context) }

    private lateinit var postsAdapter: PostsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPostBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.posts = viewModel

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        postsAdapter = PostsAdapter()
        binding.rvPosts.apply {
            adapter = postsAdapter
        }
        viewModel.dataPosts.observe(viewLifecycleOwner, Observer {
            postsAdapter.submitList(it)
        })
    }

}