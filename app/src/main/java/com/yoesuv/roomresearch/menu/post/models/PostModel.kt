package com.yoesuv.roomresearch.menu.post.models

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "posts")
data class PostModel(
    @ColumnInfo(name = "user_id") val userId: Int?,
    @ColumnInfo(name = "id") @PrimaryKey val id: Int?,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "body") val body: String?
)