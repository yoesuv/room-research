package com.yoesuv.roomresearch.menu.splash.views

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.yoesuv.roomresearch.MainActivity
import com.yoesuv.roomresearch.R
import com.yoesuv.roomresearch.databinding.ActivitySplashBinding
import com.yoesuv.roomresearch.menu.splash.viewmodels.SplashViewModel

class SplashActivity: AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding
    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        binding.lifecycleOwner = this

        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
        binding.splash = viewModel

        viewModel.initAppData {
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }
    }

}