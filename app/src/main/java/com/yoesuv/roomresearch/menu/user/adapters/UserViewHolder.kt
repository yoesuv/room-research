package com.yoesuv.roomresearch.menu.user.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yoesuv.roomresearch.databinding.ItemUserBinding
import com.yoesuv.roomresearch.menu.user.models.UserModel
import com.yoesuv.roomresearch.menu.user.viewmodels.ItemUserViewModel

class UserViewHolder(val binding: ItemUserBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(userModel: UserModel) {
        binding.user = ItemUserViewModel(userModel)
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): UserViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: ItemUserBinding = ItemUserBinding.inflate(inflater, parent, false)
            return UserViewHolder(binding)
        }
    }

}