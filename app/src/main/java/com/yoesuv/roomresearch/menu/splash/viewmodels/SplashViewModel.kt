package com.yoesuv.roomresearch.menu.splash.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.yoesuv.roomresearch.data.db.DbPostRepository
import com.yoesuv.roomresearch.data.db.DbUserRepository
import com.yoesuv.roomresearch.networks.AppRepository
import com.yoesuv.roomresearch.utils.logError

class SplashViewModel(application: Application) : AndroidViewModel(application) {

    private val repo = AppRepository(viewModelScope)
    private val dbPostRepo = DbPostRepository(application.applicationContext, viewModelScope)
    private val dbUserRepo = DbUserRepository(application.applicationContext, viewModelScope)

    fun initAppData(onSuccess:() -> Unit) {
        dbPostRepo.deleteAllPost()
        dbUserRepo.deleteAllUser()
        repo.initAppData({ posts, users ->
            posts?.forEach { post ->
                dbPostRepo.insertPost(post)
            }
            users?.forEach { user ->
                dbUserRepo.insertUser(user)
            }
            onSuccess()
        },{
            logError("SplashViewModel # failed get posts & users")
        })
    }

}