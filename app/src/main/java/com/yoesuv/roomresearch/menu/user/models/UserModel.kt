package com.yoesuv.roomresearch.menu.user.models

import androidx.annotation.Keep
import androidx.room.*

/**
 * https://stackoverflow.com/a/56581731
 * https://stackoverflow.com/a/50711841
 */
@Keep
@Entity(tableName = "users")
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
data class UserModel(
    @PrimaryKey @ColumnInfo(name="id") val id: Int?,
    @ColumnInfo(name="name") val name: String?,
    @ColumnInfo(name="username") val username: String?,
    @ColumnInfo(name="email") val email: String?,
    @Embedded(prefix = "user_address") val address: AddressModel?,
    @ColumnInfo(name="phone") val phone: String?,
    @ColumnInfo(name="website") val website: String?,
    @Embedded(prefix = "user_company") val company: CompanyModel?
)