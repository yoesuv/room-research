package com.yoesuv.roomresearch.menu.user.models

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "company")
data class CompanyModel(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "catch_phrase") val catchPhrase: String?,
    @ColumnInfo(name = "bs") val bs: String?
)