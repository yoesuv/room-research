package com.yoesuv.roomresearch.menu.post.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.yoesuv.roomresearch.menu.post.models.PostModel
import com.yoesuv.roomresearch.utils.AdapterCallback

class PostsAdapter: ListAdapter<PostModel, PostViewHolder>(AdapterCallback.diffPostsCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}