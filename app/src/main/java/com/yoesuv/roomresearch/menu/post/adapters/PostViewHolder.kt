package com.yoesuv.roomresearch.menu.post.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yoesuv.roomresearch.databinding.ItemPostBinding
import com.yoesuv.roomresearch.menu.post.models.PostModel
import com.yoesuv.roomresearch.menu.post.viewmodels.ItemPostViewModel

class PostViewHolder(val binding: ItemPostBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(postModel: PostModel) {
        binding.post = ItemPostViewModel(postModel)
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): PostViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: ItemPostBinding = ItemPostBinding.inflate(inflater, parent, false)
            return PostViewHolder(binding)
        }
    }
}