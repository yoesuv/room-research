package com.yoesuv.roomresearch.menu.user.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yoesuv.roomresearch.data.db.DbUserRepository
import com.yoesuv.roomresearch.menu.user.models.UserModel

class UsersViewModel(context: Context): ViewModel() {

    private val dbUserRepo = DbUserRepository(context, viewModelScope)

    var dataUsers:MutableLiveData<MutableList<UserModel>> = MutableLiveData()

    init {
        dbUserRepo.getUsers {
            dataUsers.postValue(it)
        }
    }

}