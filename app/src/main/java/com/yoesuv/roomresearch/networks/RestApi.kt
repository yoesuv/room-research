package com.yoesuv.roomresearch.networks

import com.yoesuv.roomresearch.menu.post.models.PostModel
import com.yoesuv.roomresearch.menu.user.models.UserModel
import retrofit2.Response
import retrofit2.http.GET

interface RestApi {

    @GET("posts")
    suspend fun posts(): Response<MutableList<PostModel>>

    @GET("users")
    suspend fun users(): Response<MutableList<UserModel>>
}