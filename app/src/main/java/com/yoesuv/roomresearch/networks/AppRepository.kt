package com.yoesuv.roomresearch.networks

import com.yoesuv.roomresearch.menu.post.models.PostModel
import com.yoesuv.roomresearch.menu.user.models.UserModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class AppRepository(private val scope: CoroutineScope) {

    private val api = ServiceFactory.getApiService(RestApi::class.java)

    fun initAppData(onSuccess:(MutableList<PostModel>?, MutableList<UserModel>?) -> Unit, onError:(Throwable) -> Unit) {
        scope.launch {
            try {
                val resultPosts = api.posts()
                val resultUsers = api.users()
                onSuccess(resultPosts.body(), resultUsers.body())
            } catch (throwable: Throwable) {
                if (throwable !is CancellationException) {
                    onError(throwable)
                }
            }
        }
    }
}