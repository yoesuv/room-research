package com.yoesuv.roomresearch.networks

import com.yoesuv.roomresearch.BuildConfig
import com.yoesuv.roomresearch.data.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ServiceFactory {

    private fun retrofitInstance(): Retrofit {

        val logging = HttpLoggingInterceptor()
        logging.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.addInterceptor(logging)

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(clientBuilder.build())
            .build()
    }

    fun <T> getApiService(service:Class<T>):T {
        return retrofitInstance().create(service)
    }
}