package com.yoesuv.roomresearch.utils

import android.util.Log
import com.yoesuv.roomresearch.BuildConfig

fun logDebug(message: String) {
    if (BuildConfig.DEBUG) Log.d("result_debug", message)
}

fun logError(message: String) {
    if (BuildConfig.DEBUG) Log.e("result_error", message)
}