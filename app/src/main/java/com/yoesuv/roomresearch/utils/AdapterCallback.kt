package com.yoesuv.roomresearch.utils

import androidx.recyclerview.widget.DiffUtil
import com.yoesuv.roomresearch.menu.post.models.PostModel
import com.yoesuv.roomresearch.menu.user.models.UserModel

object AdapterCallback {

    val diffPostsCallback = object : DiffUtil.ItemCallback<PostModel>() {
        override fun areItemsTheSame(oldItem: PostModel, newItem: PostModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: PostModel, newItem: PostModel): Boolean {
            return oldItem.id == newItem.id
        }
    }

    val diffUsersCallback = object : DiffUtil.ItemCallback<UserModel>() {
        override fun areItemsTheSame(oldItem: UserModel, newItem: UserModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: UserModel, newItem: UserModel): Boolean {
            return oldItem.id == newItem.id
        }
    }
}