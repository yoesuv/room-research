package com.yoesuv.roomresearch.utils

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yoesuv.roomresearch.menu.post.viewmodels.PostsViewModel
import com.yoesuv.roomresearch.menu.user.viewmodels.UsersViewModel

/**
 * https://stackoverflow.com/a/50374088
 */
class ViewModelFragmentFactory(private val params: Any): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == PostsViewModel::class.java) {
            return PostsViewModel(params as Context) as T
        } else if (modelClass == UsersViewModel::class.java) {
            return UsersViewModel(params as Context) as T
        }
        return super.create(modelClass)
    }

}