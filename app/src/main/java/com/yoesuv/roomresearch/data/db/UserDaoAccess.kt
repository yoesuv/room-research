package com.yoesuv.roomresearch.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yoesuv.roomresearch.menu.user.models.UserModel

@Dao
interface UserDaoAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(userModel: UserModel)

    @Query("SELECT * FROM users")
    suspend fun users(): MutableList<UserModel>

    @Query("DELETE FROM users")
    suspend fun deleteAllUsers()

}