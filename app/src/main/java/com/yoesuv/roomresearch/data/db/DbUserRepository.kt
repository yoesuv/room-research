package com.yoesuv.roomresearch.data.db

import android.content.Context
import com.yoesuv.roomresearch.menu.user.models.UserModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class DbUserRepository(context: Context, private val scope: CoroutineScope) {

    private val dbUser = AppDatabase.getInstance(context)?.userDaoAccess()

    fun insertUser(userModel: UserModel) {
        scope.launch {
            dbUser?.insertUser(userModel)
        }
    }

    fun getUsers(users:(MutableList<UserModel>?) -> Unit) {
        scope.launch {
            users(dbUser?.users())
        }
    }

    fun deleteAllUser() {
        scope.launch {
            dbUser?.deleteAllUsers()
        }
    }
}