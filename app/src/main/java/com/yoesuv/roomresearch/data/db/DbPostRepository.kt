package com.yoesuv.roomresearch.data.db

import android.content.Context
import com.yoesuv.roomresearch.menu.post.models.PostModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class DbPostRepository(context: Context, private val scope: CoroutineScope) {

    private val dbPost = AppDatabase.getInstance(context)?.postDaoAccess()

    fun insertPost(postModel: PostModel) {
        scope.launch {
            dbPost?.insertPost(postModel)
        }
    }

    fun getPosts(posts:(MutableList<PostModel>?) -> Unit){
        scope.launch {
            posts(dbPost?.posts())
        }
    }

    fun deleteAllPost() {
        scope.launch {
            dbPost?.deleteAllPost()
        }
    }
}