package com.yoesuv.roomresearch.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.yoesuv.roomresearch.data.DB_NAME
import com.yoesuv.roomresearch.menu.post.models.PostModel
import com.yoesuv.roomresearch.menu.user.models.AddressModel
import com.yoesuv.roomresearch.menu.user.models.CompanyModel
import com.yoesuv.roomresearch.menu.user.models.GeoModel
import com.yoesuv.roomresearch.menu.user.models.UserModel

/** REFERENCES
 * https://stackoverflow.com/a/50105730
 */
@Database(entities = [PostModel::class, UserModel::class, CompanyModel::class, AddressModel::class, GeoModel::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun postDaoAccess(): PostDaoAccess

    abstract fun userDaoAccess(): UserDaoAccess

    companion object {

        @Volatile
        private var instance: AppDatabase? = null
        @Synchronized
        fun getInstance(context: Context): AppDatabase? {
            if (instance == null) {
                instance = create(context)
            }
            return instance
        }

        private fun create(context: Context): AppDatabase{
            return Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME).build()
        }
    }

}