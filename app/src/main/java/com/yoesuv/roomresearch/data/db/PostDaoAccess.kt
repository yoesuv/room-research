package com.yoesuv.roomresearch.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yoesuv.roomresearch.menu.post.models.PostModel

@Dao
interface PostDaoAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPost(postModel: PostModel)

    @Query("SELECT user_id, id, title, body FROM posts")
    suspend fun posts(): MutableList<PostModel>

    @Query("DELETE FROM posts")
    suspend fun deleteAllPost()

}