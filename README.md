## Android Room Research 1.0.1 ##

save data from API to local db  
API from ```http://jsonplaceholder.typicode.com/posts``` & ```http://jsonplaceholder.typicode.com/users```  
download apk file [here](https://www.dropbox.com/s/yzuirhxuxgxhpmt)

MVVM Pattern, Navigation Component, Room & Lifecycle

#### Screenshot ####
| Splash | Posts | Users |
| :---: | :---: | :---: |
| ![splash](https://i.imgur.com/8PZBV1t.jpg) | ![posts](https://i.imgur.com/hznrUzn.jpg) | ![users](https://i.imgur.com/p2taxI5.jpg) |

#### Libraries ####
- [Coroutines](https://github.com/Kotlin/kotlinx.coroutines)
- [Moshi](https://github.com/square/moshi)
- [OkHttp](https://github.com/square/okhttp)
- [Retrofit](https://github.com/square/retrofit)
